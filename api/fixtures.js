const mongoose = require('mongoose');
const config = require("./config");
const User = require("./models/User");
const Task = require("./models/Task");
const {STATUS_NEW, STATUS_IN_PROGRESS} = require("../constants");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user1, user2, user3] = await User.create({
    username: 'John'
  }, {
    username: 'Robert'
  }, {
    username: 'Bill'
  });

  await Task.create({
    title: 'Buy Milk',
    status: STATUS_NEW,
    user: user1
  }, {
    title: 'Save World',
    status: STATUS_IN_PROGRESS,
    user: user3
  }, {
    title: 'Do homework',
    status: STATUS_NEW,
  });

  await mongoose.connection.close();
};

run().catch(e => console.error(e));