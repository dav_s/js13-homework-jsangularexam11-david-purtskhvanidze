const path = require('path');
const fs = require("fs").promises;
const express = require('express');
const multer = require('multer');
const { nanoid } = require('nanoid');
const config = require('../config');
const Item = require("../models/Item");
const mongoose = require("mongoose");


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try {
        const items = await Item.find().populate("user");

        return res.send(items);
    } catch (e) {
        next(e);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const item = await Item.findById(req.params.id);

        if (!item) {
            return res.status(404).send({message: 'Not found'});
        }

        return res.send(item);
    } catch (e) {
        next(e);
    }
});

router.post('/', upload.single('image'), async (req, res, next) => {
    try {
        if (!req.body.title || !req.body.description || !req.body.price || !req.body.image || !req.body.category || !req.body.user) {
            return res.status(400).send({message: 'Title, description, price, image, category and user are required'});
        }

        const itemData = {
            title: req.body.title,
            description: req.body.description,
            price: parseFloat(req.body.price),
            image: req.file.filename,
            category: req.body.category,
            user: req.body.user,
        };

        const item = new Item(itemData);

        await item.save();

        return res.send({message: 'Created new product', id: item._id});
    } catch (e) {
        next(e);
    }
});

module.exports = router;