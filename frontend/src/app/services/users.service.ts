import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiUserData, LoginUserData, RegisterUserData, User } from '../models/user.model';
import { environment, environment as env } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private http: HttpClient) { }

  registerUser(userData: RegisterUserData) {
    return this.http.post<User>(env.apiUrl + '/users', userData);
  }

  login(userData: LoginUserData) {
    return this.http.post<User>(env.apiUrl + '/users/sessions', userData);
  }

  logout(token: string) {
    return this.http.delete(env.apiUrl + '/users/sessions', {
      headers: new HttpHeaders({'Authorization': token})
    });
  }

  getUsers() {
    return this.http.get<ApiUserData[]>(environment.apiUrl + '/users').pipe(
      map(response => {
        return response.map(userData => {
          return new User(
            userData._id,
            userData.email,
            userData.token,
            userData.displayName,
            userData.phone
          );
        });
      })
    );
  }
}
