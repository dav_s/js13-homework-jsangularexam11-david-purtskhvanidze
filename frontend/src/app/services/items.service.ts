import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiItemData, Item, ItemData } from '../models/item.model';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(private http: HttpClient) { }

  getItems() {
    return this.http.get<ApiItemData[]>(environment.apiUrl + '/items').pipe(
      map(response => {
        return response.map(itemData => {
          return new Item(
            itemData._id,
            itemData.title,
            itemData.description,
            itemData.price,
            itemData.image,
            itemData.category,
            itemData.user,
          );
        });
      })
    );
  }

  createItem(itemData: ItemData) {
    const formData = new FormData();

    Object.keys(itemData).forEach(key => {
      if (itemData[key] !== null) {
        formData.append(key, itemData[key]);
      }
    });

    return this.http.post(environment.apiUrl + '/items', formData);
  }
}
