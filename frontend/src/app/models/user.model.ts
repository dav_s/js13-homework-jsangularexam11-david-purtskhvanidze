export class User {
  constructor(
    public id: string,
    public displayName: string,
    public email: string,
    public phone: string,
    public token: string,
  ) {}
}

export interface User {
  _id: string,
  displayName: string,
  email: string,
  phone: string,
  token: string,
}

export interface ApiUserData {
  _id: string,
  email: string,
  token: string,
  displayName: string,
  phone: string
}

export interface RegisterUserData {
  displayName: string,
  email: string,
  phone: string,
  password: string
}

export interface LoginUserData {
  displayName: string,
  email: string,
  phone: string,
  password: string,
}

export interface FieldError {
  message: string
}

export interface RegisterError {
  errors: {
    password: FieldError,
    email: FieldError
  }
}

export interface LoginError {
  error: string
}
