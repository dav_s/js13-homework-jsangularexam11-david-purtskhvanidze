export class Item {
  constructor(
    public id: string,
    public title: string,
    public description: string,
    public price: number,
    public image: string,
    public category: string,
    public user: string,
  ) {}
}

export interface ItemData {
  [key: string]: any;
  title: string;
  description: string;
  price: number;
  image: File | null;
  category: string;
  user: string;
}

export interface ApiItemData {
  _id: string,
  title: string,
  description: string,
  price: number,
  image: string,
  category: string,
  user: string
}
