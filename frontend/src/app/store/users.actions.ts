import { createAction, props } from '@ngrx/store';
import { LoginError, LoginUserData, RegisterError, RegisterUserData, User } from '../models/user.model';
import { Item } from '../models/item.model';

export const registerUserRequest = createAction(
  '[Users] Register Request',
  props<{userData: RegisterUserData}>()
);
export const registerUserSuccess = createAction(
  '[Users] Register Success',
  props<{user: User}>()
);
export const registerUserFailure = createAction(
  '[Users] Register Failure',
  props<{error: null | RegisterError}>()
);
export const loginUserRequest = createAction(
  '[Users] Login Request',
  props<{userData: LoginUserData}>()
);
export const loginUserSuccess = createAction(
  '[Users] Login Success',
  props<{user: User}>()
);
export const loginUserFailure = createAction(
  '[Users] Login Failure',
  props<{error: null | LoginError}>()
);
export const fetchUsersRequest = createAction('[Users] Fetch Request');
export const fetchUsersSuccess = createAction(
  '[Users] Fetch Success',
  props<{users: User[]}>()
);
export const fetchUsersFailure = createAction(
  '[Users] Fetch Failure',
  props<{error: string}>()
);
export const logoutUser = createAction('[Users] Logout');
export const logoutUserRequest = createAction('[Users] Server Logout Request');
