import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ItemsService } from '../services/items.service';
import {
  createItemFailure,
  createItemRequest,
  createItemSuccess,
  fetchItemsFailure,
  fetchItemsRequest,
  fetchItemsSuccess
} from './items.actions';
import { mergeMap, map, catchError, of, tap } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class ItemsEffects {
  fetchItems = createEffect(() => this.actions.pipe(
    ofType(fetchItemsRequest),
    mergeMap(() => this.itemsService.getItems().pipe(
      map(items => fetchItemsSuccess({items})),
      catchError(() => of(fetchItemsFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  createItem = createEffect(() => this.actions.pipe(
    ofType(createItemRequest),
    mergeMap(({itemData}) => this.itemsService.createItem(itemData).pipe(
      map(() => createItemSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createItemFailure({error: 'Wrong data'})))
    ))
  ));

  constructor(
    private actions: Actions,
    private itemsService: ItemsService,
    private router: Router
  ) {}
}
