import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { ItemData } from '../../models/item.model';
import { createItemRequest } from '../../store/items.actions';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.sass']
})
export class AddComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<string | null>;
  user: Observable<any>;

  constructor(
    private store: Store<AppState>
  ) {
    this.loading = store.select(state => state.items.createLoading);
    this.error = store.select(state => state.items.createError);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    console.log(this.user);
  }

  onSubmit() {
    const itemData: ItemData = this.form.value;
    this.store.dispatch(createItemRequest({itemData}));
  }
}
